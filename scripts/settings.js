const debouncedReload = debounce(() => window.location.reload(), 100);

export const registerSettings = function () {
    game.settings.register("anvelA5E", "allactions", {
        name: "Display Action/Bonus checkbox on trackers?",
        hint: "By default only the Reaction is listed on the auxiliary window and the pop-up combat tracker. Enabling this also allows to keep track of the main/bonus action.",
        scope: "user",
        type: Boolean,
        default: false,
        config: true,
        onChange: debouncedReload
    });
}
