import { dnd5e, i18n, Utils, ItemUtils } from "../../betterrolls5e/scripts/utils/index.js";
import { libWrapper } from "./libWrapper.js";
import { CustomRoll, CustomItemRoll } from "../../betterrolls5e/scripts/custom-roll.js";
import { getSettings } from "../../betterrolls5e/scripts/settings.js";
import { d20Roll } from "../../../../systems/dnd5e/module/dice.js";
import ActorSkillConfig from "../../../../systems/dnd5e/module/apps/skill-config.js";
import ItemSheet5e from "../../../../systems/dnd5e/module/item/sheet.js";
import Item5e from "../../../../systems/dnd5e/module/item/entity.js";
import { Tidy5eItemSheet } from "../../tidy5e-sheet/scripts/tidy5e-item.js";
import Actor5e from "../../../../systems/dnd5e/module/actor/entity.js";


Hooks.once('ready', async function() {

	patchCoreFunctions();
	
	console.log('Anvel\'s A5E | Patched Rolls');

});

function itemRollA5E(defaultRoll, options) {
	// Handle options, same defaults as core 5e
	options = mergeObject({
		configureDialog: true,
		createMessage: true,
		event
	}, options, { recursive: false });
	const { rollMode, createMessage, vanilla } = options;
	const altKey = options.event?.altKey;
	const item = this;

	// Case - If the image button should roll a vanilla roll, UNLESS vanilla is defined and is false
	const { imageButtonEnabled, altSecondaryEnabled } = getSettings();
	if (vanilla || (!imageButtonEnabled && vanilla !== false) || (altKey && !altSecondaryEnabled)) {
		return defaultRoll.bind(item)(options);
	}

	const preset = altKey ? 1 : 0;
	const card = window.BetterRolls.rollItem(item, { preset, event: options.event });
	card.settings.skillId = "";
	card.settings.expertisedice = "";
	card.settings.specialties = "";
	return card.toMessage({ rollMode, createMessage });
}

async function actorRollSkillA5E(original, skillId, options) {
	if (options?.chatMessage === false || options?.vanilla) {
		return original.call(this, skillId, options);
	}

	const roll = await original.call(this, skillId, {
		...options,
		fastForward: true,
		chatMessage: false,
		...Utils.getRollState(options),
	});
	
	var fullRoll = await CustomRoll._fullRollActor(this, i18n(dnd5e.skills[skillId]), roll);
	var skills = fullRoll._actor.data.data.skills;
	
	fullRoll.settings.skillId = skillId;
	fullRoll.settings.expertisedice = skills[skillId].bonuses.expertisedice;
	fullRoll.settings.specialties = skills[skillId].bonuses.specialties;

	return fullRoll;
};

const patchCoreFunctions = function () {
	if (!libWrapper.is_fallback && !libWrapper.version_at_least?.(1, 4, 0)) {
		Hooks.once("ready", () => {
			const version = "v1.4.0.0";
			ui.notifications.error(i18n("br5e.error.libWrapperMinVersion", { version }));
		});

		return;
	}

	const actorProto = "CONFIG.Actor.documentClass.prototype";
	libWrapper.register("anvelA5E", "CONFIG.Item.documentClass.prototype.roll", itemRollA5E, "WRAPPER", {chain: false});
	libWrapper.register("anvelA5E", `${actorProto}.rollSkill`, actorRollSkillA5E, "MIXED");
}

ItemUtils.getSpellComponents = function(item) {
    const { vocal, somatic, material, one, two, three, onethree, stance } = item.data.data.components;
		
		let componentString = "";

		if (vocal) {
			componentString += i18n("br5e.chat.abrVocal");
		}

		if (somatic) {
			componentString += i18n("br5e.chat.abrSomatic");
		}

		if (material) {
			const materials = item.data.data.materials;
			componentString += i18n("br5e.chat.abrMaterial");

			if (materials.value) {
				const materialConsumption = materials.consumed ? i18n("br5e.chat.consumedBySpell") : ""
				componentString += ` (${materials.value}` + ` ${materialConsumption})`;
			}
		}
		
		if (one) {
			componentString += " 1 Ex. ";
		}
		
		if (two) {
			componentString += " 2 Ex. ";
		}
		
		if (three) {
			componentString += " 3 Ex. ";
		}
		
		if (onethree) {
			componentString += " 1-3 Ex. ";
		}
		
		if (stance) {
			componentString += " Stance ";
		}

		return componentString || null;
};

ItemUtils.getBaseCritRoll = function(baseFormula) {
	if (!baseFormula) return null;

	const critFormula = baseFormula.replace(/[+-]+\s*(?:@[a-zA-Z0-9.]+|[0-9]+(?![Dd]))/g,"").concat();
	let critRoll = new Roll(baseFormula);
	if (critRoll.terms.length === 1 && typeof critRoll.terms[0] === "number") {
		return null;
	}

	return critRoll;
};



CustomItemRoll.repeat = async function (options={}) {
	adsadbreak;
		if (!this.canRepeat()) return;

		// Show error messages if the item/actor was deleted
		const subject = await this.getItem() ?? await this.getActor();
		if ((this.itemId || this.actorId) && !subject) {
			const actor = await this.getActor();
			const message = actor ? i18n("br5e.error.noItemWithId") : i18n("br5e.error.noActorWithId");
			ui.notifications.error(message);
			throw new Error(message);
		}

		const invalidFields = ["description", "desc"];
		const fields = duplicate(this.fields.filter(f => !invalidFields.includes(f[0])));
		const params = duplicate(this.params);
		params.consume = false;
		params.rollState = Utils.getRollState(options);

		const newRoll = new CustomItemRoll(subject, params, fields);
		await newRoll.toMessage();
		return newRoll;
	}

Object.defineProperty(ActorSkillConfig, 'defaultOptions', {
	get: function() {
		return foundry.utils.mergeObject(DocumentSheet.defaultOptions, {
			classes: ["dnd5e"],
			template: "modules/anvelA5E/templates/skill-config.html",
			width: 500,
			height: "auto"
		});
	}
});

Object.defineProperty(Tidy5eItemSheet.prototype, 'template', {
	get: function() {
		if(`${this.item.data.type}.html` == 'spell.html')
		{
			return "modules/anvelA5E/templates/spell.html";
		}
		else
		{
			const path = "systems/dnd5e/templates/items/";
			return `${path}/${this.item.data.type}.html`;
		}
	}
});