import { registerSettings } from "./settings.js";
import { AnvelA5ETools } from "./auxtool.js";

let expertiseDiceHTML;

let A5ETool;

Hooks.once('init', async function() {

	registerSettings();
	
	A5ETool = new AnvelA5ETools;
	
	$.get("modules/anvelA5E/templates/expertiseDiceTemplate.html").done(function(data) {
		expertiseDiceHTML = data;
	});
	
	console.log('Anvel\'s A5E | Init Done');

});

$(document).on('click', '.expertiseDiceButton', function () {
	
	console.log('Anvel\'s A5E | Rolling Expertise Dice'); 
	rollExpertise($(this).data('size'))
	
});

function rollExpertise(size) {
	
	var speaker = ChatMessage.getSpeaker();
	console.log('Anvel\'s A5E | Rolling Expertise D' + size); 
	const roll = new Roll('1d' + size);
	roll.roll();
	roll.toMessage({
		flavor: "D" + size + " Expertise Dice",
		speaker: speaker,
	});
	
};

Hooks.on("renderCombat", function (force, options) {
	console.log("Anvel\'s A5E | renderCombat");
	renderCombatActionsHTML();
});

async function renderCombatActionsHTML() {
	var windowData = {
		display_actions: game.settings.get("anvelA5E", "allactions")
	};
	
	let html = await renderTemplate("modules/anvelA5E/templates/combatActions.html", windowData);
	$('#combat-popout .token-actions').remove();
	$(html).appendTo("#combat-tracker .combatant");
};

Hooks.on("renderCombatTracker", function (force, options) {
	console.log("Anvel\'s A5E | renderCombatTracker");
	renderCombatTrackerActionsHTML();	
});

async function renderCombatTrackerActionsHTML() {
	var windowData = {
		display_actions: game.settings.get("anvelA5E", "allactions")
	};
	let html = await renderTemplate("modules/anvelA5E/templates/combatActions.html", windowData);
	$('#combat-popout .token-actions').remove();
	$(html).appendTo("#combat-popout #combat-tracker .combatant");
	updateActionsUsed();
};

$(document).on('click', '.token-action', function (e) {
	$(this).toggleClass('available');
	
	var action_type = "data.actions." + $(this).data('action-type');
	var combatant_id = $(this).closest('.combatant').data('combatant-id');
	var target_actor = null;
	var actor_id = null;
	
	if(typeof combatant_id !== 'undefined')
	{
		game.combats.forEach(function(combat, index) {
			var combatant = combat.data.combatants.get(combatant_id);
			if(typeof combatant !== 'undefined')
			{
				target_actor = combatant._actor;
			}
		});
	}
	else
	{
		actor_id = $('.anvelA5EAuxWindow').data('actor-id');
		target_actor = game.actors.get(actor_id);
	}	
	
	if(target_actor.data.data.actions[$(this).data('action-type')] == 0)
	{		
		target_actor.data.data.actions[$(this).data('action-type')] = 1;
		target_actor.update({"data.actions": target_actor.data.data.actions});
		//console.log("Anvel\'s A5E | Used");
	}
	else
	{
		target_actor.data.data.actions[$(this).data('action-type')] = 0;
		target_actor.update({"data.actions": target_actor.data.data.actions});
		//console.log("Anvel\'s A5E | Available");
	}
	
});

function autoUpdateActionsUser() {

    updateActionsUsed();

    setTimeout(autoUpdateActionsUser, 5000);
}

autoUpdateActionsUser();

function updateActionsUsed() {
	
	$('#combat-popout .combatant').each(function( index ) {
		
		var combatant_id = $(this).data('combatant-id');
		var target_actor = null;
		
		game.combats.forEach(function(combat, index) {

			var combatant = combat.data.combatants.get(combatant_id);

			if(typeof combatant !== 'undefined')
			{
				target_actor = combatant._actor;
			}

		});
		
		if(typeof target_actor !== 'undefined')
		{
			if(target_actor.isOwner)
			{
				var actions = target_actor.data.data.actions;
				
				if(actions.action == 1)
				{
					$('.combatant[data-combatant-id="' + combatant_id + '"] .token-action-main').removeClass('available');
				}
				else
				{
					$('.combatant[data-combatant-id="' + combatant_id + '"] .token-action-main').addClass('available');
				}
				
				if(actions.bonus == 1)
				{
					$('.combatant[data-combatant-id="' + combatant_id + '"] .token-action-bonus').removeClass('available');
				}
				else
				{
					$('.combatant[data-combatant-id="' + combatant_id + '"] .token-action-bonus').addClass('available');
				}
				
				if(actions.reaction == 1)
				{
					$('.combatant[data-combatant-id="' + combatant_id + '"] .token-action-reaction').removeClass('available');
				}
				else
				{
					$('.combatant[data-combatant-id="' + combatant_id + '"] .token-action-reaction').addClass('available');
				}
			}
			else
			{
					$('.combatant[data-combatant-id="' + combatant_id + '"] .token-actions').remove();
			}
		}
		
	});
	
};

Hooks.on("preCreateCombat", function (combat) {
    console.log("Anvel\'s A5E | preCreateCombat");
});
Hooks.on("createCombat", function (combat) {
    console.log("Anvel\'s A5E | createCombat");
});
Hooks.on("updateCombat", function (combat, update, options, userId) {
    console.log("Anvel\'s A5E | updateCombat");

	if(game.users.current.isGM)
	{	
	
		var combatant_id = combat.current.combatantId;
		var combatant = combat.data.combatants.get(combatant_id);
		
		if(typeof combatant !== 'undefined')
		{
			var target_actor = combatant._actor;
			
			if(typeof target_actor !== 'undefined')
			{
				target_actor.data.data.actions.action = 0;
				target_actor.data.data.actions.bonus = 0;
				target_actor.data.data.actions.reaction = 0;
				target_actor.update({"data.actions": target_actor.data.data.actions});
			}
		}
		
	}
	
	console.log("Anvel\'s A5E | updateCombatDone");
	
});
Hooks.on("preDeleteCombat", function (combat) {
    console.log("Anvel\'s A5E | preDeleteCombat");
});
Hooks.on("deleteCombat", function (combat) {
    console.log("Anvel\'s A5E | deleteCombat");
});

Hooks.on("createCombatant", function (combatant, options, userId) {
    console.log("Anvel\'s A5E | createCombatant");
	
	if(game.users.current.isGM)
	{
		if(typeof combatant !== 'undefined')
		{
			var target_actor = combatant._actor;
			
			if(typeof target_actor !== 'undefined')
			{
				target_actor.data.data.actions.action = 0;
				target_actor.data.data.actions.bonus = 0;
				target_actor.data.data.actions.reaction = 0;
				target_actor.update({"data.actions": target_actor.data.data.actions});
			}
		}
	}

	console.log("Anvel\'s A5E | createCombatantDone");
});
Hooks.on("updateCombatant", function (combat, combatant, options) {
    console.log("Anvel\'s A5E | updateCombatant");
});
Hooks.on("updateActor", function (actor, options) {
    console.log("Anvel\'s A5E | updateActor");
	
	if($('.anvelA5EAuxWindow').data('actor-id') == actor.data._id)
	{
		if(A5ETool.enabled)
		{
			A5ETool.render();
		}
	}
	
	updateActionsUsed();
});
Hooks.on("preNextTurn", function (combatant, options) {
	console.log("Anvel\'s A5E | preNextTurn");
});

Hooks.on("preCreateChatMessage", function (data, options, renderOptions, userId) {
	console.log("Anvel\'s A5E | preCreateChatMessage");	
});

Hooks.on("createChatMessage", function (data, options, userId) {
	console.log("Anvel\'s A5E | createChatMessage");
	
	var expertiseRolls = [getRandomNumber(1, 4), getRandomNumber(1, 6), getRandomNumber(1, 8), getRandomNumber(1, 10), getRandomNumber(1, 12)];
	if(data.isAuthor)
	{
		data.setFlag("anvelA5E", "expertiseRolls", expertiseRolls);
	}
});

Hooks.on("renderChatMessage", function (data, html, options) {
	console.log("Anvel\'s A5E | renderChatMessage");
	
	var wrapper = html.find('.dice-result').first();
	wrapper.append(expertiseDiceHTML);
	
	var expertiseRolls = data.getFlag("anvelA5E", "expertiseRolls");
	var specialties = data.getFlag("anvelA5E", "specialties");	
	var expertiseDice = data.getFlag("anvelA5E", "expertiseDice");	
	
	if(typeof expertiseRolls !== 'undefined')
	{
		html.find('.expertiseD4').html(expertiseRolls[0]);
		html.find('.expertiseD6').html(expertiseRolls[1]);
		html.find('.expertiseD8').html(expertiseRolls[2]);
		html.find('.expertiseD10').html(expertiseRolls[3]);
		html.find('.expertiseD12').html(expertiseRolls[4]);
	}	
	
	if(typeof expertiseDice !== 'undefined')
	{
		if(expertiseDice != '')
		{
			var expertiseHeader = html.find('.expertise-header').first();
			var expertiseHeaderContent = expertiseHeader.html();
			expertiseHeader.html(expertiseHeaderContent + ' (' + getExpertiseDiceText(expertiseDice) + ')');
		}
	}
	else
	{
		if(typeof data.BetterRoll !== 'undefined')
		{
			expertiseDice = data.BetterRoll.settings.expertisedice;
			
			if(typeof expertiseDice !== 'undefined')
			{
				if(expertiseDice != '')
				{
					var expertiseHeader = html.find('.expertise-header').first();
					var expertiseHeaderContent = expertiseHeader.html();
					expertiseHeader.html(expertiseHeaderContent + ' (' + getExpertiseDiceText(expertiseDice) + ')');
				}
				
				if(data.isAuthor)
				{
					data.setFlag("anvelA5E", "expertiseDice", data.BetterRoll.settings.expertisedice);
				}
			}
		}
	}
	
	if(typeof specialties !== 'undefined')
	{
		if(specialties != '')
		{
			var specialtiesHeader = html.find('.item-name').first();
			var specialtiesHeaderContent = specialtiesHeader.html();
			specialtiesHeader.html(specialtiesHeaderContent + ' (' + specialties + ')');
		}
	}
	else
	{
		if(typeof data.BetterRoll !== 'undefined')
		{
			specialties = data.BetterRoll.settings.specialties;
			
			if(typeof specialties !== 'undefined')
			{
				if(specialties != '')
				{
					var specialtiesHeader = html.find('.item-name').first();
					var specialtiesHeaderContent = specialtiesHeader.html();
					specialtiesHeader.html(specialtiesHeaderContent + ' (' + specialties + ')');
				}
				
				if(data.isAuthor)
				{
					data.setFlag("anvelA5E", "specialties", data.BetterRoll.settings.specialties);
				}
			}
		}
	}
	

});

function getExpertiseDiceText(n) {
	if(n <= 0) { return "none"; }
	else if (n == 1) { return "d4"; }
	else if (n == 2) { return "d6"; }
	else if (n == 3) { return "d8"; }
	else if (n == 4) { return "d10"; }
	else if (n == 5) { return "d12"; }
};

function getRandomNumber(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

Hooks.on("controlToken", function (token, currentlySelected) {
	console.log("Anvel\'s A5E | controlToken");
});

Hooks.on("renderActorSheet", function (sheet, sheetwindow, actor) {
	console.log("Anvel\'s A5E | renderActorSheet");
	renderActorSheetHTML(actor);

});

async function renderActorSheetHTML (actor) {
	var sheet_actor_id = '#actor-' + actor.actor._id;
	
	var exertion = {current: actor.data.attributes.exertion, max: actor.data.attributes.maxexertion};
	var exhaustion = {current: actor.data.attributes.exhaustion};
	var strife = {current: actor.data.attributes.strife};
	
	let htmlOrigin = await renderTemplate("modules/anvelA5E/templates/tidyOrigin.html", actor);
	let htmlExertion = await renderTemplate("modules/anvelA5E/templates/tidyExertion.html", exertion);
	let htmlExhaustion = await renderTemplate("modules/anvelA5E/templates/tidyExhaustion.html", exhaustion);
	let htmlStrife = await renderTemplate("modules/anvelA5E/templates/tidyStrife.html", strife);
	
	htmlOrigin = $(htmlOrigin);
	
	
	$(sheet_actor_id + ' .exhaustion-container .exhaust-level').append('<li data-elvl="7">7</li>');	
	$(sheet_actor_id + ' .tidy5e-header .profile-wrap').append($(htmlExertion));	
	$(sheet_actor_id + ' .exhaustion-container .note').html($(htmlExhaustion));
	$(sheet_actor_id + ' .exhaustion-container').clone().addClass('strife-container').insertAfter($(sheet_actor_id + ' .exhaustion-container'));
	
	$(sheet_actor_id + ' .strife-container').removeClass (function (index, className) {
		return (className.match (/(^|\s)level-\S+/g) || []).join(' ');
	});
	
	$(sheet_actor_id + ' .strife-container').addClass('level-' + actor.data.attributes.strife);				
	$(sheet_actor_id + ' .strife-container .level-display').html(actor.data.attributes.strife);
	$(sheet_actor_id + ' .strife-container .note').html($(htmlStrife));
	

	htmlOrigin.on('focusout', '#' + actor.actor._id + '-race', function (e) {		
		var current_actor = game.actors.get($(this).data('actor'));
		current_actor.update({"data.details.race": $('#' + current_actor.id + '-race').html()});
	});
	
	htmlOrigin.on('focusout', '#' + actor.actor._id + '-culture', function (e) {	
		var current_actor = game.actors.get($(this).data('actor'));
		current_actor.update({"data.details.culture": $('#' + current_actor.id + '-culture').html()});
	});
	
	htmlOrigin.on('focusout', '#' + actor.actor._id + '-background', function (e) {	
		var current_actor = game.actors.get($(this).data('actor'));
		current_actor.update({"data.details.background": $('#' + current_actor.id + '-background').html()});
	});
	
	htmlOrigin.on('focusout', '#' + actor.actor._id + '-destiny', function (e) {	
		var current_actor = game.actors.get($(this).data('actor'));
		current_actor.update({"data.details.destiny": $('#' + current_actor.id + '-destiny').html()});
	});
	
	htmlOrigin.on('focusout', '#' + actor.actor._id + '-alignment', function (e) {	
		var current_actor = game.actors.get($(this).data('actor'));
		current_actor.update({"data.details.alignment": $('#' + current_actor.id + '-alignment').html()});
	});
	
	htmlOrigin.find('.actor-size-select .size-label').on('click', function(){
		let currentSize = $(this).data('size');
		$(this).closest('ul').toggleClass('active').find('ul li[data-size="'+currentSize+'"]').addClass("current");
	});
	htmlOrigin.find('.actor-size-select .size-list li').on('click', async (event) => {
		let value = event.target.dataset.size;
		var current_actor = game.actors.get($(event.currentTarget).parent().data('actor'));
		current_actor.update({"data.traits.size": value});
		htmlOrigin.find('.actor-size-select').toggleClass('active');
	});
	
	$(sheet_actor_id + ' .origin-summary').html(htmlOrigin);
	
	
	
	/*
	$('.actor-size-select .size-label').on('click', function(){
		let currentSize = $(this).data('size');
		$(this).closest('ul').toggleClass('active').find('ul li[data-size="'+currentSize+'"]').addClass("current");
	});
	$('.actor-size-select .size-list li').on('click', async (event) => {
		let value = event.target.dataset.size;
		var current_actor = game.actors.get($(this).data('actor'));
		current_actor.update({"data.traits.size": value});
		$('.actor-size-select').toggleClass('active');
	});*/

}





$(document).on('click', '.exhaustion-container:not(.strife-container) .exhaust-level li[data-elvl="7"]', async function (e) {

	let elvl = $(this).data('elvl');
	let actor_id = $(this).closest('.tidy5e.sheet.actor').attr('id').substring(6);
	let actor = game.actors.get(actor_id);
	
	event.preventDefault();
	let target = event.currentTarget;
	let value = elvl;
	let data = actor.data.data;
	await actor.update({"data.attributes.exhaustion": value});
	
});

$(document).on('click', '.exhaustion-container.strife-container .exhaust-level li', async function (e) {

	let elvl = $(this).data('elvl');
	let sheet_actor_id = $(this).closest('.tidy5e.sheet.actor').attr('id');
	let actor_id = $(this).closest('.tidy5e.sheet.actor').attr('id').substring(6);
	let actor = game.actors.get(actor_id);
	
	event.preventDefault();
	let target = event.currentTarget;
	let value = elvl;
	let data = actor.data.data;
	await actor.update({"data.attributes.strife": value});
	
	$(sheet_actor_id + ' .strife-container .level-display').html(value);
	
	$(sheet_actor_id + ' .strife-container').removeClass (function (index, className) {
		return (className.match (/(^|\s)level-\S+/g) || []).join(' ');
	});
	
	$(sheet_actor_id + ' .strife-container').addClass('level-' + value);
	
});

$(document).on('click', '.exhaustion-container:not(.strife-container) .exhaust-level li[data-elvl="7"]', async function (e) {

	let elvl = $(this).data('elvl');
	let actor_id = $(this).closest('.tidy5e.sheet.actor').attr('id').substring(6);
	let actor = game.actors.get(actor_id);
	
	event.preventDefault();
	let target = event.currentTarget;
	let value = elvl;
	let data = actor.data.data;
	await actor.update({"data.attributes.exhaustion": value});
	
});

/* ------------------------------------ */
/* Additional Hooks                     */
/* ------------------------------------ */
Hooks.on('getSceneControlButtons', controls => {

	let tokenButton = controls.find(b => b.name == "token");
	if (tokenButton) {
		tokenButton.tools.push(
			{
				name: "anvela5e",
				title: "A5E Auxiliary Tool",
				icon: "fas fa-dragon",
				onClick: () => {
					A5ETool.enabled = true;
					A5ETool.render();
					tokenButton.activeTool = "select";
				}
			}
		);
	};
});

