export class AnvelA5ETools extends Application {

    constructor(object, options) {
        super(options);
		this.enabled = false;
		this.actor = null;

        // Update dialog display on changes to token selection
        Hooks.on("controlToken", async (object, controlled) => {
            let x = await canvas.tokens.controlled;
			if(this.enabled)
			{
				this.render();
			}
        });
    }
	
	static get defaultOptions() {
        const options = super.defaultOptions;
        return options;
    }
	
	/**
    * Render the outer application wrapper
    * @return {Promise.<HTMLElement>}   A promise resolving to the constructed jQuery object
    * @private
    */
    async render() {

        // Gather basic application data
        const classes = this.options.classes;
        const windowData = {
            id: this.id,
            classes: classes.join(" "),
            appId: this.appId,
            title: this.title,
            headerButtons: this._getHeaderButtons()
        };
		
		var actor = game.actors.get(ChatMessage.getSpeaker().actor);
		
		setActor(windowData, actor);		
		setManeuverDC(windowData, actor);
		setUsedActions(windowData, actor);
		setExertion(windowData, actor);
		setFatigue(windowData, actor);
		
        // Render the template and return the promise
        let html = await renderTemplate("modules/anvelA5E/templates/anvelA5EAux.html", windowData);
        html = $(html);

        // Activate header button click listeners after a slight timeout to prevent immediate interaction
        setTimeout(() => {
            html.find(".header-button").click(event => {
                event.preventDefault();
                const button = windowData.headerButtons.find(b => event.currentTarget.classList.contains(b.class));
                button.onclick(event);
            });
        }, 500);

        // Make the outer window draggable
        const header = html.find('header')[0];3
        new Draggable(this, html, header, this.options.resizable);

        // Make the outer window minimizable
        if (this.options.minimizable) {
            header.addEventListener('dblclick', this._onToggleMinimize.bind(this));
        }

        // Set the outer frame z-index
        if (Object.keys(ui.windows).length === 0) _maxZ = 100 - 1;
        html.css({ zIndex: Math.min(++_maxZ, 9999) });

		html.css("top", this.position.top + "px");
		html.css("left", this.position.left + "px");

        // Return the outer frame
		
		setEvents(html, actor);
		
		$('.anvelA5EAuxWindow').remove();
		jQuery(document.body).prepend(html);
		
        return html;
    }

    activateListeners(html) {
        super.activateListeners(html);
    }
};

export function setActor (windowData, actor) {
	
	windowData.actor_id = actor.data._id;
	windowData.actor_name = ChatMessage.getSpeaker().alias;
	windowData.actor_image = actor.data.img;
	
};

export function setManeuverDC (windowData, actor) {
	
	var str = actor.data.data.abilities.str.mod;
	var dex = actor.data.data.abilities.dex.mod;
	var prof = actor.data.data.prof;
	
	var dc = 8 + prof + Math.max(str, dex);
	windowData.maneuverDC = dc;
	
};

export function setUsedActions (windowData, actor) {
	
	windowData.action = "";
	windowData.bonus = "";
	windowData.reaction = "";
	windowData.display_actions = game.settings.get("anvelA5E", "allactions");
	
	if(actor.data.data.actions.action == 0)
	{
		windowData.action = "available";
	}
	
	if(actor.data.data.actions.bonus == 0)
	{
		windowData.bonus = "available";
	}
	
	if(actor.data.data.actions.reaction == 0)
	{
		windowData.reaction = "available";
	}
};

export function setExertion (windowData, actor) {
	
	windowData.A5EExertionCurrent = actor.data.data.attributes.exertion;
	windowData.A5EExertionMax = actor.data.data.attributes.maxexertion;
	
};

export function setFatigue (windowData, actor) {
	
	windowData.A5EExhaustionValue = actor.data.data.attributes.exhaustion;
	windowData.A5EStrifeValue = actor.data.data.attributes.strife;
		
};

export function setEvents (html, actor) {
	
	html.on('click', '.header-button.close', function (e) {
		html.slideUp( "fast", function() {
			this.enabled = false;
			this.remove();
		});
	});
	
	html.on('click', '#decreaseExhaustion', function (e) {
		if(actor.data.data.attributes.exhaustion > 0){
			actor.update({"data.attributes.exhaustion": (parseInt($('#A5EExhaustionValue').html()) - 1)});
		}
		
	});
	
	html.on('click', '#increaseExhaustion', function (e) {
		actor.update({"data.attributes.exhaustion": (parseInt($('#A5EExhaustionValue').html()) + 1)});
		
	});
	
	html.on('click', '#decreaseStrife', function (e) {
		if(actor.data.data.attributes.strife > 0){
			actor.update({"data.attributes.strife": (parseInt($('#A5EStrifeValue').html()) - 1)});
		}
		
	});
	
	html.on('click', '#increaseStrife', function (e) {
		actor.update({"data.attributes.strife": (parseInt($('#A5EStrifeValue').html()) + 1)});			
	});
	
	html.on('focusout', '#A5EExertionCurrent', function (e) {
		actor.update({"data.attributes.exertion": $('#A5EExertionCurrent').val()});
	});
	
	html.on('focusout', '#A5EExertionMax', function (e) {
		actor.update({"data.attributes.maxexertion": $('#A5EExertionMax').val()});
	});
};